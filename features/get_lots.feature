Feature: Get Lots
  As a user
  Such that I can know in advance where can I park my car
  I want to get a list of closest parking lots

  Scenario: Getting lots via web page (with confirmation)
    Given the following parking locations are existing
          | address | price	 | freeLots | distance |
          | Turu 2  | 1.0    | 3        | 1.1      |
          | Turu 3  | 1.1    | 5        | 1.0      |
    And I want get the closest parking to "Juhan Liivi 2, Tartu"
    And I open the web page
    And I enter the location address
    When I summit the request
    Then I should receive results

  Scenario: Getting lots via web page (with confirmation)
    Given the following parking locations are existing
          | address | price	 | freeLots | distance |
          | Turu 2  | 1.0    | 3        | 1.1      |
          | Turu 3  | 1.1    | 5        | 1.0      |
    And I want get the closest parking to "Narva mnt 25, Tartu"
    And I open the web page
    And I enter the location address
    When I summit the request
    Then I should receive the list of parking lots nearby