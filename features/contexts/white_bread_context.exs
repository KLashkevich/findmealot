defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers

  alias FindMeALot.{Repo, Lot}

  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)
    %{}
  end
  scenario_starting_state fn state ->
    Hound.start_session
    Ecto.Adapters.SQL.Sandbox.checkout(FindMeALot.Repo)
    Ecto.Adapters.SQL.Sandbox.mode(FindMeALot.Repo, {:shared, self()})
    %{}
  end
  scenario_finalize fn _status, _state -> 
    Ecto.Adapters.SQL.Sandbox.checkin(FindMeALot.Repo)
    #Hound.end_session
    nil
  end

  given_ ~r/^the following parking locations are existing$/, fn state, %{table_data: table} ->
    table
    |> Enum.map(fn lot -> Lot.changeset(%Lot{}, lot) end)
    |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
    {:ok, state}
  end

  and_ ~r/^I want get the closest parking to "(?<location>[^"]+)"$/,
  fn state, %{location: location} -> #CHANGED
    #{:ok, state}
    {:ok, state |> Map.put(:location, location)}
  end

  and_ ~r/^I open the web page$/, fn state -> #CHANGED
    navigate_to "/"
    {:ok, state}
  end

  and_ ~r/^I enter the location address$/, fn state ->
    fill_field({:id, "app-input0"}, state[:location])
    {:ok, state}
  end

  when_ ~r/^I summit the request$/, fn state ->
    click({:id, "app-submit"})
    {:ok, state}
  end

  then_ ~r/^I should receive results$/, fn state ->
    assert visible_in_page?(~r/Search Results/)
    {:ok, state}
  end

  then_ ~r/^I should receive the list of parking lots nearby$/, fn state ->
    assert element?(:id, "app-results-table")
    {:ok, state}
  end
end
