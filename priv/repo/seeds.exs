alias FindMeALot.Repo
alias FindMeALot.Lot

Repo.insert!(%Lot{address: "Turu 2", price: 1.0, freeLots: 30, distance: 2.0})
Repo.insert!(%Lot{address: "Vanemuise 15", price: 0.8, freeLots: 15, distance: 1.2})
Repo.insert!(%Lot{address: "Raatuse 21", price: 0.5, freeLots: 20, distance: 2.5})
