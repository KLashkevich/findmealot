defmodule FindMeALot.Repo.Migrations.CreateLots do
  use Ecto.Migration

  def change do
    create table(:lots) do
      add :address, :string
      add :price, :float
      add :freeLots, :integer
      add :distance, :float

      timestamps()
    end

  end
end
