defmodule FindMeALotWeb.API.LotsControllerTest do
    use FindMeALotWeb.ConnCase

    alias FindMeALot.{Repo, Lot}

    test "GET /api/lots/all", %{conn: conn} do
      Repo.insert!(%Lot{address: "Turu 2", price: 1.0, freeLots: 3, distance: 1.1})
      Repo.insert!(%Lot{address: "Turu 3", price: 1.0, freeLots: 1, distance: 1.1})
  
      response =
          conn
          |> get(lots_path(conn, :get_all_lots))
          |> json_response(201)

      expected = 2
      assert length(Map.get(response, "lots")) == expected
  
    end

    test "GET /api/lots/location", %{conn: conn} do
        Repo.insert!(%Lot{address: "Turu 2", price: 1.0, freeLots: 3, distance: 1.1})
        Repo.insert!(%Lot{address: "Turu 3", price: 1.0, freeLots: 1, distance: 1.1})
  
        request = %{"location" => "Liivi 2"}
    
        response =
            conn
            |> get(lots_path(conn, :get_lots_for_location, request))
            |> json_response(201)
    
        #IO.inspect response
        expected = 2
        assert length(Map.get(response, "lots")) == expected
    
      end

    #   test "Booking rejection", %{conn: conn} do
    #     Repo.insert!(%Taxi{status: "busy"})
    #     conn = post conn, "/bookings", %{booking: [pickup_address: "Liivi 2", dropoff_address: "Lõunakeskus"]}
    #     conn = get conn, redirected_to(conn)
    #     assert html_response(conn, 200) =~ ~r/At present, there is no taxi available!/
    #   end
    
    #   test "Booking aceptance", %{conn: conn} do
    #     Repo.insert!(%Taxi{status: "available"})
    #     conn = post conn, "/bookings", %{booking: [pickup_address: "Liivi 2", dropoff_address: "Lõunakeskus"]}
    #     conn = get conn, redirected_to(conn)
    #     assert html_response(conn, 200) =~ ~r/Your taxi will arrive in \d+ minutes/
    #   end
  
  end