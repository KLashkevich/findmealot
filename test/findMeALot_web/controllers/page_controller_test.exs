defmodule FindMeALotWeb.PageControllerTest do
  use FindMeALotWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "Welcome to FindMeALot!"
  end
end
