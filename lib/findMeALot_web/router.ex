defmodule FindMeALotWeb.Router do
  use FindMeALotWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", FindMeALotWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  #Other scopes may use custom stacks.
  scope "/api", FindMeALotWeb do
    pipe_through :api

    get "/lots/all", API.LotsController, :get_all_lots
    get "/lots/location", API.LotsController, :get_lots_for_location
  end
end
