defmodule FindMeALotWeb.PageController do
  use FindMeALotWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
