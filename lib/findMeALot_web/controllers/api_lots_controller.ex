defmodule FindMeALotWeb.API.LotsController do
    use FindMeALotWeb, :controller

    import Ecto.Changeset
    alias FindMeALot.{Repo, Lot}

    def get_all_lots(conn, _params) do
        lots = Repo.all(Lot)
        put_status(conn, 201)
            |> json(%{lots: lots})
    end

    def get_lots_for_location(conn, %{"location" => location}) do
        lots = Repo.all(Lot)
        put_status(conn, 201)
            |> json(%{lots: lots})
        # Add some nice code here
        # case Repo.get_by(Lot, code: code) do
        #     nil ->
        #         put_status(conn, 400)
        #         |> json(%{msg: "Rate was not found"})
        #     record ->
        #         put_status(conn, 201)
        #         |> json(%{id: record.id, code: record.code, name: record.name, 
        #             value: record.value, start_value: record.start_value, stop_value: record.stop_value})
        # end
    end
end