defmodule FindMeALot.Lot do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Poison.Encoder, only: [:address, :distance, :freeLots, :price]}
  schema "lots" do
    field :address, :string
    field :distance, :float
    field :freeLots, :integer
    field :price, :float

    timestamps()
  end

  @doc false
  def changeset(lot, attrs \\ %{}) do
    lot
    |> cast(attrs, [:address, :price, :freeLots, :distance])
    |> validate_required([:address, :price, :freeLots, :distance])
  end
end
