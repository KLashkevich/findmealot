use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :findMeALot, FindMeALotWeb.Endpoint,
  http: [port: 4001],
  server: true

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :findMeALot, FindMeALot.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "findmealot_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

config :hound, driver: "chrome_driver"
config :findMeALot, sql_sandbox: true

config :findMeALot, gmaps_api_key: "AIzaSyCbgUFZTeMyjurXiU3RTUlbLkUSuGRlw1c"
